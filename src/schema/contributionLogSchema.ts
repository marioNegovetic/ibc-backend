import { gql } from "apollo-server-express";
import { IbcEntity } from "../database/entities/IbcEntity";
import { GraphQLID } from "../utils/tsTypes";

export interface IContributionLog {
  id: GraphQLID;
  date: Date;
  accomplishment: string;
  businessImpact: string;
  ibcId: IbcEntity;
}

export interface IContributionLogIbc {
  id: GraphQLID;
  date: Date;
  accomplishment: string;
  businessImpact: string;
}

export interface ICreateContributionLogInput {
  input: {
    accomplishment: string;
    businessImpact: string;
    ibcId: IbcEntity;
  };
}

export interface IGetContributionLogInput {
  filter: {
    id: GraphQLID;
  };
}

export interface IUpdateContributionLogInput {
  input: {
    id: GraphQLID;
    accomplishment: string;
    businessImpact: string;
  };
}
export interface IDeleteContributionLogInput {
  input: {
    id: GraphQLID;
  };
}
const contributionLogSchema = gql`
  type ContributionLog {
    id: ID!
    ibc: Ibc
    date: Date
    accomplishment: String
    businessImpact: String
  }

  input GetContributionLogInput {
    id: ID!
  }

  type Query {
    getContributionLogs: [ContributionLog]
    getContributionLog(filter: GetContributionLogInput!): ContributionLog
  }

  input CreateContributionLogInput {
    accomplishment: String
    businessImpact: String
    ibcId: ID!
  }

  input UpdateContributionLogInput {
    id: ID!
    accomplishment: String
    businessImpact: String
  }
  input DeleteContributionLogInput {
    id: ID!
  }
  type Mutation {
    createContributionLog(input: CreateContributionLogInput!): ContributionLog!
    updateContributionLog(input: UpdateContributionLogInput!): ContributionLog!
    deleteContributionLog(input: DeleteContributionLogInput!): Boolean
  }
`;

export default contributionLogSchema;
