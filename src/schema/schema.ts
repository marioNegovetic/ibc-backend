import userSchema from './userSchema';
import schemaScalars from './schemaScalars';
import ibcSchema from './ibcSchema';
import performanceGoalSchema from './performanceGoalSchema';
import developmentGoalSchema from './developmentGoalSchema';
import contributionLogSchema from './contributionLogSchema';

const typeDefs = [
	schemaScalars,
	userSchema,
	ibcSchema,
	performanceGoalSchema,
	developmentGoalSchema,
	contributionLogSchema,
];

export default typeDefs;
