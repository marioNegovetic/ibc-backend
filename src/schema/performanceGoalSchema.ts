import { gql } from "apollo-server-express";
import { IbcEntity } from "../database/entities/IbcEntity";
import { GraphQLID } from "../utils/tsTypes";

export interface IPerformanceGoal {
  id: GraphQLID;
  ibcId: IbcEntity;
  goalDescription: string;
  startDate: Date;
  endDate: Date;
  supportNeeded?: string;
  midYearStatus: string;
  eoyYearStatus: string;
}

export interface IPerformanceGoalIbc {
  id: GraphQLID;
  goalDescription: string;
  startDate: Date;
  endDate: Date;
  supportNeeded?: string;
  midYearStatus: string;
  eoyYearStatus: string;
}

export interface ICreatePerformanceGoalInput {
  input: {
    ibcId: IbcEntity;
    goalDescription: string;
    startDate: Date;
    endDate: Date;
    supportNeeded: string;
    midYearStatus: string;
    eoyYearStatus: string;
  };
}

export interface IGetPerformanceGoalInput {
  filter: {
    id: GraphQLID;
  };
}
export interface IGetPerformanceGoalsInput {
  filter: {
    ibcId: GraphQLID;
  };
}

export interface IDeletePerformanceGoalInput {
  input: {
    id: GraphQLID;
  };
}

export interface IUpdatePerformanceGoalInput {
  input: {
    id: GraphQLID;
    goalDescription?: string;
    startDate?: Date;
    endDate?: Date;
    supportNeeded?: string;
    midYearStatus?: string;
    eoyYearStatus?: string;
  };
}

const performanceGoalSchema = gql`
  type PerformanceGoal {
    id: ID!
    ibc: Ibc
    goalDescription: String!
    startDate: Date!
    endDate: Date!
    supportNeeded: String
    midYearStatus: String
    eoyYearStatus: String
  }

  input GetPerformanceGoalInput {
    id: ID!
  }
  input GetPerformanceGoalsInput {
    ibcId: ID!
  }

  type Query {
    getPerformanceGoals(filter: GetPerformanceGoalsInput!): [PerformanceGoal!]
    getPerformanceGoal(filter: GetPerformanceGoalInput!): PerformanceGoal
  }

  input CreatePerformanceGoalInput {
    ibcId: ID!
    goalDescription: String!
    startDate: Date!
    endDate: Date!
    supportNeeded: String
    midYearStatus: String
    eoyYearStatus: String
  }

  input UpdatePerformanceGoalInput {
    id: ID!
    goalDescription: String
    startDate: Date
    endDate: Date
    supportNeeded: String
    midYearStatus: String
    eoyYearStatus: String
  }

  input DeletePerformanceGoalInput {
    id: ID!
  }

  type Mutation {
    createPerformanceGoal(input: CreatePerformanceGoalInput!): PerformanceGoal!
    updatePerformanceGoal(input: UpdatePerformanceGoalInput!): PerformanceGoal!
    deletePerformanceGoal(input: DeletePerformanceGoalInput!): Boolean
  }
`;

export default performanceGoalSchema;
