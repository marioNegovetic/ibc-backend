import { gql } from "apollo-server-express";
import { StatusEntity } from "../database/entities/StatusEntity";
import { UserEntity } from "../database/entities/UserEntity";
import { GraphQLID } from "../utils/tsTypes";
export interface IIbc {
  id: GraphQLID;
  user: UserEntity;
  reviewer: UserEntity;
  ibcYear: number;
  midReviewDate: Date;
  eoyReviewDate: Date;
  status: StatusEntity;
  annualSalary: number;
}

export interface ICreateIbcInput {
  input: {
    ibcYear: number;
    annualSalary: number;
  };
}
export interface IUpdateIbcInput {
  input: {
    id: GraphQLID;
    reviewer: UserEntity;
    ibcYear: number;
    status: StatusEntity;
    midReviewDate: Date;
    eoyReviewDate: Date;
    annualSalary: number;
  };
}
export interface IDeleteIbcInput {
  input: {
    id: GraphQLID;
  };
}
export interface IGetIbcInput {
  filter: {
    id: GraphQLID;
  };
}

const ibcSchema = gql`
  type Ibc {
    id: ID!
    user: User
    reviewer: User
    ibcYear: Int
    midReviewDate: Date
    eoyReviewDate: Date
    status: Status
    performanceGoals: [PerformanceGoal]!
    developmentGoals: [DevelopmentGoal]!
    contributionLogs: [ContributionLog]!
    annualSalary: Int
  }
  type Status {
    id: ID!
    name: String
  }

  input GetIbcInput {
    id: ID!
  }

  type Query {
    getIbcs: [Ibc!]
    getIbc(filter: GetIbcInput!): Ibc
    getIbcsReviewer: [Ibc]
    getIbcsPractitioner: [Ibc]
  }

  input CreateIbcInput {
    ibcYear: Int
    annualSalary: Int
  }

  input UpdateIbcInput {
    id: ID!
    ibcYear: Int
    reviewer: ID
    status: ID
    midReviewDate: Date
    eoyReviewDate: Date
    annualSalary: Int
  }

  input DeleteIbcInput {
    id: ID!
  }

  type Mutation {
    createIbc(input: CreateIbcInput!): Ibc!
    updateIbc(input: UpdateIbcInput!): Ibc!
    deleteIbc(input: DeleteIbcInput!): Boolean
  }
`;

export default ibcSchema;
