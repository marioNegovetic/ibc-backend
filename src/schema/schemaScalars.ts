import { gql } from "apollo-server-express";

const schemaScalars = gql`
  scalar Date
`;

export default schemaScalars;
