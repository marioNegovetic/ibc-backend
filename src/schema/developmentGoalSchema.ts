import { gql } from "apollo-server-express";
import { IbcEntity } from "../database/entities/IbcEntity";
import { GraphQLID } from "../utils/tsTypes";

export interface IDevelopmentGoal {
  id: GraphQLID;
  ibcId: IbcEntity;
  goalDescription: string;
  startDate: Date;
  endDate: Date;
  supportNeeded: string;
  midYearStatus: string;
  eoyYearStatus: string;
}

export interface IDevelopmentGoalIbc {
  id: GraphQLID;
  goalDescription: string;
  startDate: Date;
  endDate: Date;
  supportNeeded: string;
  midYearStatus: string;
  eoyYearStatus: string;
}

export interface ICreateDevelopmentGoalInput {
  input: {
    ibcId: IbcEntity;
    goalDescription: string;
    startDate: Date;
    endDate: Date;
    supportNeeded: string;
    midYearStatus: string;
    eoyYearStatus: string;
  };
}

export interface IGetDevelopmentGoalInput {
  filter: {
    id: GraphQLID;
  };
}

export interface IGetDevelopmentGoalsInput {
  filter: {
    ibcId: GraphQLID;
  };
}

export interface IDeleteDevelopmentGoalInput {
  input: {
    id: GraphQLID;
  };
}

export interface IUpdateDevelopmentGoalInput {
  input: {
    id: GraphQLID;
    goalDescription?: string;
    startDate?: Date;
    endDate?: Date;
    supportNeeded?: string;
    midYearStatus?: string;
    eoyYearStatus?: string;
  };
}

const developmentGoalSchema = gql`
  type DevelopmentGoal {
    id: ID!
    ibc: Ibc
    goalDescription: String!
    startDate: Date!
    endDate: Date!
    supportNeeded: String
    midYearStatus: String
    eoyYearStatus: String
  }

  input GetDevelopmentGoalInput {
    id: ID!
  }

  input GetDevelopmentGoalsInput {
    ibcId: ID!
  }

  type Query {
    getDevelopmentGoals(filter: GetDevelopmentGoalsInput!): [DevelopmentGoal!]
    getDevelopmentGoal(filter: GetDevelopmentGoalInput!): DevelopmentGoal
  }

  input CreateDevelopmentGoalInput {
    ibcId: ID!
    goalDescription: String!
    startDate: Date!
    endDate: Date!
    supportNeeded: String
    midYearStatus: String
    eoyYearStatus: String
  }

  input UpdateDevelopmentGoalInput {
    id: ID!
    goalDescription: String
    startDate: Date
    endDate: Date
    supportNeeded: String
    midYearStatus: String
    eoyYearStatus: String
  }

  input DeleteDevelopmentGoalInput {
    id: ID!
  }

  type Mutation {
    createDevelopmentGoal(input: CreateDevelopmentGoalInput!): DevelopmentGoal!
    updateDevelopmentGoal(input: UpdateDevelopmentGoalInput!): DevelopmentGoal!
    deleteDevelopmentGoal(input: DeleteDevelopmentGoalInput!): Boolean
  }
`;

export default developmentGoalSchema;
