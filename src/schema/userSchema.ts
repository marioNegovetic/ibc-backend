import { gql } from "apollo-server-express";
import { GraphQLID } from "../utils/tsTypes";

export interface IUser {
  id: GraphQLID;
  name: string;
  email: string;
  password: string;
  serviceLine: string;
  position: string;
  squadLead: string;
  major: string;
  dateAdded: Date;
  isAdmin: boolean;
  isReviewer: boolean;
  activeUser: boolean;
}
export interface IToken {
  token: string;
}

export interface ICreateUserInput {
  input: {
    name: string;
    email: string;
    password: string;
    serviceLine: string;
    position: string;
    squadLead: string;
    major: string;
    isAdmin: boolean;
    isReviewer: boolean;
    activeUser: boolean;
  };
}

export interface IUpdateUserInput {
  input: {
    id: GraphQLID;
    name: string;
    email: string;
    password: string;
    serviceLine: string;
    position: string;
    squadLead: string;
    major: string;
    isAdmin: boolean;
    isReviewer: boolean;
    activeUser: boolean;
  };
}
export interface IChangePassword {
  input: {
    oldPassword: string;
    newPassword: string;
    confirmPassword: string;
  };
}

export interface IGetUserInput {
  filter: {
    id: GraphQLID;
  };
}

export interface IUserLogin {
  input: {
    email: string;
    password: string;
  };
}

const userSchema = gql`
  type User {
    id: ID!
    name: String!
    email: String!
    password: String!
    serviceLine: String!
    position: String!
    squadLead: String!
    major: String!
    dateAdded: Date!
    isAdmin: Boolean
    isReviewer: Boolean
    activeUser: Boolean!
  }

  type Token {
    token: String!
  }

  input GetUserInput {
    id: ID!
  }

  type Query {
    getUsers: [User!]
    getUser(filter: GetUserInput!): User
    getReviewers: [User]
  }

  input CreateUserInput {
    name: String!
    email: String!
    password: String!
    serviceLine: String!
    position: String!
    squadLead: String!
    major: String!
    isAdmin: Boolean!
    isReviewer: Boolean!
    activeUser: Boolean!
  }

  input UpdateUserInput {
    id: ID!
    name: String
    email: String
    password: String
    serviceLine: String
    position: String
    squadLead: String
    major: String
    isAdmin: Boolean
    isReviewer: Boolean
    activeUser: Boolean
  }

  input LoginUser {
    email: String!
    password: String!
  }

  input ChangePassword {
    oldPassword: String
    newPassword: String
    confirmPassword: String
  }

  type Mutation {
    createUser(input: CreateUserInput!): User!
    updateUser(input: UpdateUserInput!): User!
    loginUser(input: LoginUser): Token!
    changePassword(input: ChangePassword!): User!
  }
`;

export default userSchema;
