import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { IbcEntity } from "./IbcEntity";

@Entity({
  name: "contributonLog",
})
export class ContributionLogEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: Date;

  @Column()
  accomplishment: string;

  @Column()
  businessImpact: string;

  @ManyToOne(() => IbcEntity, (ibc) => ibc.contributionLogs, {
    onDelete: "CASCADE",
    eager: true,
  })
  ibc: IbcEntity;
}
