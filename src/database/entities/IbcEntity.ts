import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
} from "typeorm";
import { ContributionLogEntity } from "./ContributionLogEntity";

import { DevelopmentGoalEntity } from "./DevelopmentGoalEntity";
import { PerformanceGoalEntity } from "./PerformanceGoalEntity";
import { StatusEntity } from "./StatusEntity";
import { UserEntity } from "./UserEntity";

@Entity({
  name: "ibc",
})
@Unique(["user", "ibcYear"])
export class IbcEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ibcYear: number;

  @Column({ nullable: true })
  midReviewDate: Date;

  @Column({ nullable: true })
  eoyReviewDate: Date;

  @Column({ nullable: true })
  annualSalary: number;

  @ManyToOne(() => StatusEntity, (status) => status.ibcs, {
    eager: true,
    nullable: false,
  })
  status: StatusEntity;

  //user
  @ManyToOne(() => UserEntity, (user) => user.userIds, {
    eager: true,
    nullable: false,
  })
  user: UserEntity;
  //reviewer
  @ManyToOne(() => UserEntity, (user) => user.reviewerIds, { eager: true })
  reviewer: UserEntity;

  @OneToMany(
    () => PerformanceGoalEntity,
    (performanceGoal) => performanceGoal.ibc
  )
  performanceGoals: PerformanceGoalEntity[];

  @OneToMany(
    () => DevelopmentGoalEntity,
    (developmentGoal) => developmentGoal.ibc
  )
  developmentGoals: DevelopmentGoalEntity[];

  @OneToMany(
    () => ContributionLogEntity,
    (contributionLog) => contributionLog.ibc
  )
  contributionLogs: ContributionLogEntity[];
}
