import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { IbcEntity } from "./IbcEntity";

@Entity({
  name: "performanceGoal",
})
export class PerformanceGoalEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  goalDescription: string;

  @Column()
  startDate: Date;

  @Column()
  endDate: Date;

  @Column()
  supportNeeded: string;

  @Column({ nullable: true })
  midYearStatus: string;

  @Column({ nullable: true })
  eoyYearStatus: string;

  @ManyToOne(() => IbcEntity, (ibc) => ibc.performanceGoals, {
    onDelete: "CASCADE",
    eager: true,
  })
  ibc: IbcEntity;
}
