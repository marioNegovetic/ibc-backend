import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { IbcEntity } from "./IbcEntity";

@Entity({
  name: "status",
})
export class StatusEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: String;

  @OneToMany(() => IbcEntity, (ibc) => ibc.status)
  ibcs: IbcEntity[];
}
