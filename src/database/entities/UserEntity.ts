import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { IbcEntity } from "./IbcEntity";

@Entity({
  name: "user",
})
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  serviceLine: string;

  @Column()
  position: string;

  @Column()
  squadLead: string;

  @Column()
  major: string;

  @CreateDateColumn()
  dateAdded: Date;

  @Column({ default: false })
  isAdmin: boolean;

  @Column({ default: false })
  isReviewer: boolean;

  @Column()
  activeUser: boolean;

  //user
  @OneToMany(() => IbcEntity, (ibc) => ibc.user)
  userIds: IbcEntity[];

  //reviewer
  @OneToMany(() => IbcEntity, (ibc) => ibc.reviewer)
  reviewerIds: IbcEntity[];
}
