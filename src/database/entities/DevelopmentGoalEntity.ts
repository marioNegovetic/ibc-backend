import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { IbcEntity } from "./IbcEntity";

@Entity({
  name: "developmentGoal",
})
export class DevelopmentGoalEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  goalDescription: string;

  @Column()
  startDate: Date;

  @Column()
  endDate: Date;

  @Column()
  supportNeeded: string;

  @Column({ nullable: true })
  midYearStatus: string;

  @Column({ nullable: true })
  eoyYearStatus: string;

  @ManyToOne(() => IbcEntity, (ibc) => ibc.developmentGoals, {
    onDelete: "CASCADE",
    eager: true,
  })
  ibc: IbcEntity;
}
