export const StatusSeed = [
  {
    name: "Not Submitted",
  },
  {
    name: "Submitted",
  },
  {
    name: "Midyear Checkpoint Completed",
  },
  {
    name: "End of year Checkpoint Completed",
  },
];
