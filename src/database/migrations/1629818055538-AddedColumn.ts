import {MigrationInterface, QueryRunner} from "typeorm";

export class AddedColumn1629818055538 implements MigrationInterface {
    name = 'AddedColumn1629818055538'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."user" ALTER COLUMN "dateAdded" SET DEFAULT now()`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."user" ALTER COLUMN "dateAdded" DROP DEFAULT`);
    }

}
