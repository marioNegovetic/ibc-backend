import {MigrationInterface, QueryRunner} from "typeorm";

export class StatusTable1630069335361 implements MigrationInterface {
    name = 'StatusTable1630069335361'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "status" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_e12743a7086ec826733f54e1d95" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ADD "statusId" integer`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ADD CONSTRAINT "FK_8216a5513fe2689cd4a7f5928d1" FOREIGN KEY ("statusId") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."ibc" DROP CONSTRAINT "FK_8216a5513fe2689cd4a7f5928d1"`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" DROP COLUMN "statusId"`);
        await queryRunner.query(`DROP TABLE "status"`);
    }

}
