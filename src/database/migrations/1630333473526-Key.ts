import {MigrationInterface, QueryRunner} from "typeorm";

export class Key1630333473526 implements MigrationInterface {
    name = 'Key1630333473526'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."ibc" DROP CONSTRAINT "FK_8216a5513fe2689cd4a7f5928d1"`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" DROP CONSTRAINT "FK_0c69f2878dd6ac5ddccf8ad9379"`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "statusId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "userId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ADD CONSTRAINT "UQ_62b61ff9d0fd0d01c2c076d9bca" UNIQUE ("userId", "ibcYear")`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ADD CONSTRAINT "FK_8216a5513fe2689cd4a7f5928d1" FOREIGN KEY ("statusId") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ADD CONSTRAINT "FK_0c69f2878dd6ac5ddccf8ad9379" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."ibc" DROP CONSTRAINT "FK_0c69f2878dd6ac5ddccf8ad9379"`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" DROP CONSTRAINT "FK_8216a5513fe2689cd4a7f5928d1"`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" DROP CONSTRAINT "UQ_62b61ff9d0fd0d01c2c076d9bca"`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "userId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "statusId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ADD CONSTRAINT "FK_0c69f2878dd6ac5ddccf8ad9379" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ADD CONSTRAINT "FK_8216a5513fe2689cd4a7f5928d1" FOREIGN KEY ("statusId") REFERENCES "status"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
