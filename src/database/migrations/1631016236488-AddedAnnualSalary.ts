import {MigrationInterface, QueryRunner} from "typeorm";

export class AddedAnnualSalary1631016236488 implements MigrationInterface {
    name = 'AddedAnnualSalary1631016236488'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."ibc" ADD "annualSalary" integer`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."ibc" DROP COLUMN "annualSalary"`);
    }

}
