import { getRepository, MigrationInterface, QueryRunner } from "typeorm";
import { StatusEntity } from "../entities/StatusEntity";
import { StatusSeed } from "../seeds/status.seed";

export class StatusData1630070292966 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await getRepository(StatusEntity).save(StatusSeed);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    //
  }
}
