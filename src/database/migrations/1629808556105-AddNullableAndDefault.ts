import {MigrationInterface, QueryRunner} from "typeorm";

export class AddNullableAndDefault1629808556105 implements MigrationInterface {
    name = 'AddNullableAndDefault1629808556105'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."user" RENAME COLUMN "active_user" TO "activeUser"`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" ALTER COLUMN "startDate" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" ALTER COLUMN "endDate" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" ALTER COLUMN "midYearStatus" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" ALTER COLUMN "eoyYearStatus" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" ALTER COLUMN "startDate" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" ALTER COLUMN "endDate" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" ALTER COLUMN "midYearStatus" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" ALTER COLUMN "eoyYearStatus" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."user" ALTER COLUMN "dateAdded" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."user" ALTER COLUMN "isAdmin" SET DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "public"."user" ALTER COLUMN "isReviewer" SET DEFAULT false`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "midReviewDate" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "midReviewDate" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "eoyReviewDate" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "eoyReviewDate" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."contributonLog" ALTER COLUMN "date" DROP DEFAULT`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."contributonLog" ALTER COLUMN "date" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "eoyReviewDate" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "eoyReviewDate" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "midReviewDate" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "public"."ibc" ALTER COLUMN "midReviewDate" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."user" ALTER COLUMN "isReviewer" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."user" ALTER COLUMN "isAdmin" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."user" ALTER COLUMN "dateAdded" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" ALTER COLUMN "eoyYearStatus" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" ALTER COLUMN "midYearStatus" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" ALTER COLUMN "endDate" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" ALTER COLUMN "startDate" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" ALTER COLUMN "eoyYearStatus" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" ALTER COLUMN "midYearStatus" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" ALTER COLUMN "endDate" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" ALTER COLUMN "startDate" SET DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "public"."user" RENAME COLUMN "activeUser" TO "active_user"`);
    }

}
