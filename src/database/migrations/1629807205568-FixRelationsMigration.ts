import {MigrationInterface, QueryRunner} from "typeorm";

export class FixRelationsMigration1629807205568 implements MigrationInterface {
    name = 'FixRelationsMigration1629807205568'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "developmentGoal" ("id" SERIAL NOT NULL, "goalDescription" character varying NOT NULL, "startDate" TIMESTAMP NOT NULL DEFAULT now(), "endDate" TIMESTAMP NOT NULL DEFAULT now(), "supportNeeded" character varying NOT NULL, "midYearStatus" character varying NOT NULL, "eoyYearStatus" character varying NOT NULL, "ibcId" integer, CONSTRAINT "PK_c9e3b80fe474f6fef2c818036d6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "performanceGoal" ("id" SERIAL NOT NULL, "goalDescription" character varying NOT NULL, "startDate" TIMESTAMP NOT NULL DEFAULT now(), "endDate" TIMESTAMP NOT NULL DEFAULT now(), "supportNeeded" character varying NOT NULL, "midYearStatus" character varying NOT NULL, "eoyYearStatus" character varying NOT NULL, "ibcId" integer, CONSTRAINT "PK_15a46a0c682a4f7b8e13200c06a" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "serviceLine" character varying NOT NULL, "position" character varying NOT NULL, "squadLead" character varying NOT NULL, "major" character varying NOT NULL, "dateAdded" TIMESTAMP NOT NULL DEFAULT now(), "isAdmin" boolean NOT NULL, "isReviewer" boolean NOT NULL, "active_user" boolean NOT NULL, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "ibc" ("id" SERIAL NOT NULL, "ibcYear" integer NOT NULL, "midReviewDate" TIMESTAMP NOT NULL DEFAULT now(), "eoyReviewDate" TIMESTAMP NOT NULL DEFAULT now(), "userId" integer, "reviewerId" integer, CONSTRAINT "PK_7bab3bc3e2ba5f6ea15931c0752" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "contributonLog" ("id" SERIAL NOT NULL, "date" TIMESTAMP NOT NULL DEFAULT now(), "accomplishment" character varying NOT NULL, "businessImpact" character varying NOT NULL, "ibcId" integer, CONSTRAINT "PK_be94551c6c80bca026b2659b6b3" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "developmentGoal" ADD CONSTRAINT "FK_cdd9ca170ff82df60327a230b06" FOREIGN KEY ("ibcId") REFERENCES "ibc"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "performanceGoal" ADD CONSTRAINT "FK_4cd366bd2595574c92401955402" FOREIGN KEY ("ibcId") REFERENCES "ibc"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "ibc" ADD CONSTRAINT "FK_0c69f2878dd6ac5ddccf8ad9379" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "ibc" ADD CONSTRAINT "FK_9b761ab537c79a8803f819ce1c7" FOREIGN KEY ("reviewerId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "contributonLog" ADD CONSTRAINT "FK_bd65b8b1d7739ab7b841dacdc7b" FOREIGN KEY ("ibcId") REFERENCES "ibc"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "contributonLog" DROP CONSTRAINT "FK_bd65b8b1d7739ab7b841dacdc7b"`);
        await queryRunner.query(`ALTER TABLE "ibc" DROP CONSTRAINT "FK_9b761ab537c79a8803f819ce1c7"`);
        await queryRunner.query(`ALTER TABLE "ibc" DROP CONSTRAINT "FK_0c69f2878dd6ac5ddccf8ad9379"`);
        await queryRunner.query(`ALTER TABLE "performanceGoal" DROP CONSTRAINT "FK_4cd366bd2595574c92401955402"`);
        await queryRunner.query(`ALTER TABLE "developmentGoal" DROP CONSTRAINT "FK_cdd9ca170ff82df60327a230b06"`);
        await queryRunner.query(`DROP TABLE "contributonLog"`);
        await queryRunner.query(`DROP TABLE "ibc"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "performanceGoal"`);
        await queryRunner.query(`DROP TABLE "developmentGoal"`);
    }

}
