import {MigrationInterface, QueryRunner} from "typeorm";

export class AddedDeleteRelation1629971011248 implements MigrationInterface {
    name = 'AddedDeleteRelation1629971011248'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" DROP CONSTRAINT "FK_cdd9ca170ff82df60327a230b06"`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" DROP CONSTRAINT "FK_4cd366bd2595574c92401955402"`);
        await queryRunner.query(`ALTER TABLE "public"."contributonLog" DROP CONSTRAINT "FK_bd65b8b1d7739ab7b841dacdc7b"`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" ADD CONSTRAINT "FK_cdd9ca170ff82df60327a230b06" FOREIGN KEY ("ibcId") REFERENCES "ibc"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" ADD CONSTRAINT "FK_4cd366bd2595574c92401955402" FOREIGN KEY ("ibcId") REFERENCES "ibc"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."contributonLog" ADD CONSTRAINT "FK_bd65b8b1d7739ab7b841dacdc7b" FOREIGN KEY ("ibcId") REFERENCES "ibc"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."contributonLog" DROP CONSTRAINT "FK_bd65b8b1d7739ab7b841dacdc7b"`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" DROP CONSTRAINT "FK_4cd366bd2595574c92401955402"`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" DROP CONSTRAINT "FK_cdd9ca170ff82df60327a230b06"`);
        await queryRunner.query(`ALTER TABLE "public"."contributonLog" ADD CONSTRAINT "FK_bd65b8b1d7739ab7b841dacdc7b" FOREIGN KEY ("ibcId") REFERENCES "ibc"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."performanceGoal" ADD CONSTRAINT "FK_4cd366bd2595574c92401955402" FOREIGN KEY ("ibcId") REFERENCES "ibc"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."developmentGoal" ADD CONSTRAINT "FK_cdd9ca170ff82df60327a230b06" FOREIGN KEY ("ibcId") REFERENCES "ibc"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
