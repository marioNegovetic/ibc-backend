import { config } from "dotenv";
config();
import "reflect-metadata";
import cors from "cors";
import express from "express";
import { ApolloServer } from "apollo-server-express";
import corsConfig from "./config/corsConfig";
import connectToDatabase from "./config/databaseConfig";
import typeDefs from "./schema/schema";
import resolvers from "./resolvers/resolver";
import { formatError } from "./errors/formatError";

const main = async (): Promise<void> => {
  const app = express();

  await connectToDatabase();

  app.use(cors(corsConfig));

  const graphqlServer = new ApolloServer({
    typeDefs: typeDefs,
    resolvers: resolvers,
    context: async ({ req }) => {
      const auth = req.headers.authorization || "";
      const token = auth.split("Bearer ")[1];
      return {
        token,
      };
    },
    formatError,
  });

  await graphqlServer.start();

  graphqlServer.applyMiddleware({ app, path: "/" });

  app.listen(process.env.PORT, () => {
    console.log(
      "App listening on",
      process.env.PORT,
      graphqlServer.graphqlPath
    );
  });
};

main();
