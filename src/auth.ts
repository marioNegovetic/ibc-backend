import { GraphQLID } from "./utils/tsTypes";
import jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { IbcEntity } from "./database/entities/IbcEntity";
import { errorDescriptor, ERRORS } from "./errors";
const SECRET = String(process.env.SECRET);

export interface TokenUser {
  id: GraphQLID;
  email: string;
  isAdmin: boolean;
  isReviewer: boolean;
  iat: number;
  exp: number;
}

export interface IStatusCheck {
  ibcId: GraphQLID;
  userId: GraphQLID;
}
const getUser = async (token: string) => {
  if (!token)
    throw new Error(errorDescriptor(ERRORS.INVALID_AUTHENTICATION_TOKEN));

  const user = jwt.verify(token, SECRET) as TokenUser;

  return user;
};
const authAdmin = (user: TokenUser) => {
  if (user.isAdmin) {
    return true;
  } else {
    return false;
  }
};
const authReviewer = (user: TokenUser) => {
  if (user.isReviewer) {
    return true;
  } else {
    return false;
  }
};

const userCheck = async (ibcId: IbcEntity, user: TokenUser) => {
  const ibc = await getRepository(IbcEntity).findOneOrFail({
    where: { id: ibcId },
  });
  if (Number(user.id) === Number(ibc.user.id)) {
    return true;
  } else {
    return false;
  }
};
const checkStatus = async (ibcId: IbcEntity) => {
  const ibc = await getRepository(IbcEntity).findOneOrFail({
    where: { id: ibcId },
  });
  return Number(ibc.status.id);
};
export { getUser, authAdmin, authReviewer, userCheck, checkStatus };
