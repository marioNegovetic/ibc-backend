/*
Remember this is GraphQL error formatter used for REST API.
So take the following explanation as it is written for GraphQL
Example: resolver : endpoint

Explanation: error object returned for each resolver should be:
{
  code: XXX,
  message: 'Description of the error.',
  key: 'UNKNOWN_ERROR'
}
On frontend you should use key, code is more of a status code
somewhat similar to http response code.
*/

export const ERRORS = {
  // ##################################################################

  BAD_REQUEST: {
    code: 400,
    message: "Bad request, possibly due malformed body.",
  },

  JSON_BODY_MALFORMED: {
    code: 400,
    message: "Body parser is unable to parse JSON request body.",
  },

  PAGE_NOT_FOUND: {
    code: 404,
    message: "404 Page not found.",
  },

  // ##################################################################

  UNAUTHORIZED: {
    code: 401,
    message: "You are unauthorized to do this action!",
  },

  DUPLICATE_IBC: {
    code: 400,
    message: "You have already created IBC for this year",
  },
  DUPLICATE_EMAIL: {
    code: 400,
    message: "Email already in use!",
  },
  IBC_SUBMITTED: {
    code: 400,
    message:
      "IBC is submitted. You are not allowed add goals to the submitted IBC.",
  },

  // ##################################################################

  DATE_FORMAT_IS_INVALID: {
    code: 400,
    message:
      "Date format is invalid, please pass the valid date formatted string.",
  },
  PASSWORD_TOO_SHORT: {
    code: 400,
    message: "Passwords must be at least 8 characters long.",
  },

  NO_DATA_ON_UPDATE: {
    code: 400,
    message: "You must specify at least one property to update!",
  },
  UNABLE_TO_CONNECT_TO_DATABASE: {
    code: 400,
    message: "Unable to connect to database.",
  },

  NOT_SUPPORTED_USE_OF_ENDPOINT: {
    code: 400,
    message:
      "You are not using the endpoint in desired way, please provide aditional parameters.",
  },

  // ##################################################################

  // Token provided is invalid.
  INVALID_AUTHENTICATION_TOKEN: {
    code: 401,
    message: "Invalid authentication token.",
  },

  INVALID_AUTHENTICATION_TOKEN_BAD_FORMAT: {
    code: 401,
    message:
      "Bad format of JWT authentication token. Did you maybe miss 'Bearer ' before 'ey...' in the request header ?",
  },

  NOT_ALLOWED_BY_CORS: {
    code: 401,
    message: "Not allowed by CORS",
  },

  INTERNAL_SERVER_ERROR: {
    code: 500,
    message: "Internal server error.",
  },

  // User data is missing when using provided auth token.
  AUTHENTICATION_DATA_IS_MISSING: {
    code: 401,
    message: "Authentication data is missing.",
  },

  // Token has expired
  AUTHENTICATION_TOKEN_HAS_EXPIRED: {
    code: 440,
    message: "Your session has timed out. Please authenticate again.",
  },

  // Token is valid but unknown user
  AUTH_VALID_BUT_LENDER_DATA_MISSING: {
    code: 401,
    message:
      "Token is valid but the lender assigned to the token does not exist.",
  },
  // ##################################################################

  // Password recovery token has expired
  RECOVERY_TOKEN_HAS_EXPIRED: {
    code: 440,
    message: "Recovery link is invalid or has expired.",
  },

  // ##################################################################

  RESOLVER_ONLY_FOR_ADMINS: {
    code: 401,
    message: "This action is only for admins.",
  },

  USER_HAS_NO_ACCESS: {
    code: 401,
    message: "Your account does not have access to this action.",
  },

  WHILE_LOOKING_FOR_SPECIFIC_USER: {
    code: 409,
    message: "Error while looking for specific user in database.",
  },

  NO_SPECIFIC_USER_OR_PASSWORD_COMBINATION: {
    code: 410,
    message: "Invalid username or password!",
  },
  WRONG_PASSWORD: {
    code: 410,
    message:
      "The password you entered does not match the currently stored password!",
  },
  NEW_PASSWORD_MATCHING_OLD: {
    code: 410,
    message: "New password cannot be the same as old!",
  },
  PASSWORDS_NOT_MATCHING: {
    code: 410,
    message: "Password confirmation doesn't match new password!",
  },
  ACCOUNT_INACTIVE: {
    code: 412,
    message: "Account is inactive. Please contact your administrator.",
  },
  // ##################################################################

  NO_SPECIFIC_USER_FOUND: {
    code: 410,
    message: "Invalid email!",
  },

  // ##################################################################

  ENDPOINT_DISABLED: {
    code: 404,
    message: "Resolver disabled.",
  },

  // ##################################################################

  // In situations where provided applicationToken is in invalid format.
  INVALID_TOKEN_FORMAT: {
    code: 400,
    message: "Invalid token format.",
  },

  // ##################################################################

  CANT_UPDATE_DATA_TO_DATABASE: {
    code: 500,
    message: "Can't update data.",
  },

  NO_DATA_FOUND: {
    code: 204,
    message: "No data found.",
  },
  // ##################################################################

  // Any error not defined in ERRORS object.
  UNKNOWN_ERROR: {
    code: 500,
    message: "Unknown server error.",
  },
};
export const errorDescriptor = (error: any) => {
  return JSON.stringify(error);
};
