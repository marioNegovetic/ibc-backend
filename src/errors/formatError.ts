import { GraphQLError } from "graphql";

export const formatError = (error: GraphQLError) => {
  let parsed = JSON.parse(error.message);
  let errorOut = {
    ...parsed,
  };
  return errorOut;
};
