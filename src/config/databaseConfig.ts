import { createConnection } from "typeorm";
import { errorDescriptor, ERRORS } from "../errors";

const connectToDatabase = async () => {
  await createConnection()
    .then((connection) => {
      console.log("Successfully connected to database!");
      return connection;
    })
    .catch((err) => {
      console.log("Unable to connect to database", err);
      throw new Error(errorDescriptor(ERRORS.UNABLE_TO_CONNECT_TO_DATABASE));
    });
};

export default connectToDatabase;
