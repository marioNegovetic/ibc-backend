import { CorsOptions } from "cors";
import { errorDescriptor, ERRORS } from "../errors";

const whitelist: string[] = [
  "https://localhost:" + process.env.PORT,
  "https://localhost:" + process.env.PORT + "/",
  "https://studio.apollographql.com",
  "http://localhost:3000",
  "https://d1ndb03xzwl01w.cloudfront.net",
  "https://ibc.iolap.academy",
];

const corsConfig: CorsOptions = {
  origin: function (origin, callback) {
    if (typeof origin === "undefined" || whitelist.includes(origin)) {
      callback(null, true);
    } else {
      callback(new Error(errorDescriptor(ERRORS.NOT_ALLOWED_BY_CORS)));
    }
  },
};

export default corsConfig;
