import { getRepository } from "typeorm";
import {
  authAdmin,
  authReviewer,
  checkStatus,
  getUser,
  userCheck,
} from "../auth";
import { DevelopmentGoalEntity } from "../database/entities/DevelopmentGoalEntity";
import { errorDescriptor, ERRORS } from "../errors";
import {
  ICreateDevelopmentGoalInput,
  IDeleteDevelopmentGoalInput,
  IGetDevelopmentGoalInput,
  IGetDevelopmentGoalsInput,
  IUpdateDevelopmentGoalInput,
} from "../schema/developmentGoalSchema";
import { IToken } from "../schema/userSchema";

const developmentGoalResolvers = {
  Query: {
    getDevelopmentGoals: async (
      _: unknown,
      args: IGetDevelopmentGoalsInput
    ) => {
      const allDevelopmentGoals = await getRepository(
        DevelopmentGoalEntity
      ).find({
        where: {
          ibc: args.filter.ibcId,
        },
      });
      return allDevelopmentGoals;
    },

    getDevelopmentGoal: async (
      _: unknown,
      args: IGetDevelopmentGoalInput,
      context: IToken
    ) => {
      getUser(context.token);
      const developmentGoal = await getRepository(
        DevelopmentGoalEntity
      ).findOne({
        where: {
          id: args.filter.id,
        },
      });

      if (developmentGoal) {
        return developmentGoal;
      } else {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_FOUND));
      }
    },
  },
  Mutation: {
    createDevelopmentGoal: async (
      _: unknown,
      args: ICreateDevelopmentGoalInput,
      context: IToken
    ) => {
      const { ibcId, goalDescription, startDate, endDate, supportNeeded } =
        args.input;
      const user = await getUser(context.token);
      const status = await checkStatus(ibcId);
      if (((await userCheck(ibcId, user)) && status === 1) || authAdmin(user)) {
        const newDevelopmentGoal = getRepository(DevelopmentGoalEntity).create({
          goalDescription,
          startDate,
          endDate,
          supportNeeded,
          ibc: ibcId,
        });
        await newDevelopmentGoal.save();
        return newDevelopmentGoal;
      } else {
        throw new Error(errorDescriptor(ERRORS.IBC_SUBMITTED));
      }
    },
    updateDevelopmentGoal: async (
      _: unknown,
      args: IUpdateDevelopmentGoalInput,
      context: IToken
    ) => {
      const {
        goalDescription,
        startDate,
        endDate,
        supportNeeded,
        midYearStatus,
        eoyYearStatus,
      } = args.input;

      const userToken = await getUser(context.token);

      if (
        !goalDescription &&
        !startDate &&
        !endDate &&
        !supportNeeded &&
        !midYearStatus &&
        !eoyYearStatus
      ) {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_ON_UPDATE));
      }

      const updateDevelopment = await getRepository(
        DevelopmentGoalEntity
      ).findOne({
        where: { id: args.input.id },
      });

      if (!updateDevelopment) {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_FOUND));
      }

      if (
        userToken.id === updateDevelopment.ibc.user.id &&
        updateDevelopment.ibc.status.id === 1
      ) {
        if (goalDescription) {
          updateDevelopment.goalDescription = goalDescription;
        }
        if (startDate) {
          updateDevelopment.startDate = startDate;
        }
        if (endDate) {
          updateDevelopment.endDate = endDate;
        }
        if (supportNeeded) {
          updateDevelopment.supportNeeded = supportNeeded;
        }
        const update = await updateDevelopment.save();
        return update;
      }
      //admin
      if (authAdmin(userToken)) {
        if (goalDescription) {
          updateDevelopment.goalDescription = goalDescription;
        }
        if (startDate) {
          updateDevelopment.startDate = startDate;
        }
        if (endDate) {
          updateDevelopment.endDate = endDate;
        }
        if (supportNeeded) {
          updateDevelopment.supportNeeded = supportNeeded;
        }
        if (midYearStatus) {
          updateDevelopment.midYearStatus = midYearStatus;
        }
        if (eoyYearStatus) {
          updateDevelopment.eoyYearStatus = eoyYearStatus;
        }
        const update = await updateDevelopment.save();

        return update;
      } else if (authReviewer(userToken)) {
        if (updateDevelopment.ibc.status.id === 2) {
          if (midYearStatus) {
            updateDevelopment.midYearStatus = midYearStatus;
          }
        } else if (updateDevelopment.ibc.status.id === 3) {
          if (eoyYearStatus) {
            updateDevelopment.eoyYearStatus = eoyYearStatus;
          }
        }

        const update = await updateDevelopment.save();

        return update;
      } else {
        throw new Error(errorDescriptor(ERRORS.UNAUTHORIZED));
      }
    },
    deleteDevelopmentGoal: async (
      _: unknown,
      args: IDeleteDevelopmentGoalInput,
      context: IToken
    ) => {
      const user = await getUser(context.token);
      const dg = await getRepository(DevelopmentGoalEntity).findOneOrFail({
        where: { id: args.input.id },
      });
      if (
        (user.id === dg.ibc.user.id && dg.ibc.status.id === 1) ||
        authAdmin(user)
      ) {
        const res = await getRepository(DevelopmentGoalEntity).delete(
          args.input.id
        );

        if (res.affected) {
          return true;
        } else {
          return false;
        }
      } else {
        throw new Error(errorDescriptor(ERRORS.UNAUTHORIZED));
      }
    },
  },
};

export default developmentGoalResolvers;
