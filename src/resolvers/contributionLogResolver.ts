import { getRepository } from "typeorm";
import { authAdmin, getUser, userCheck } from "../auth";
import { ContributionLogEntity } from "../database/entities/ContributionLogEntity";
import { errorDescriptor, ERRORS } from "../errors";
import {
  ICreateContributionLogInput,
  IDeleteContributionLogInput,
  IGetContributionLogInput,
  IUpdateContributionLogInput,
} from "../schema/contributionLogSchema";
import { IToken } from "../schema/userSchema";

const contributioLogResolvers = {
  Query: {
    getContributionLog: async (
      _: unknown,
      args: IGetContributionLogInput,
      context: IToken
    ) => {
      const user = await getUser(context.token);
      const contributionLog = await getRepository(
        ContributionLogEntity
      ).findOneOrFail({
        where: { id: args.filter.id },
      });

      if (contributionLog) {
        return contributionLog;
      } else {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_FOUND));
      }
    },
    getContributionLogs: async (_: unknown, args: IGetContributionLogInput) => {
      const contributionLogs = await getRepository(ContributionLogEntity).find({
        where: { ibc: args.filter.id },
      });
      return contributionLogs;
    },
  },
  Mutation: {
    createContributionLog: async (
      _: unknown,
      args: ICreateContributionLogInput,
      context: IToken
    ) => {
      const { ibcId, accomplishment, businessImpact } = args.input;
      const user = await getUser(context.token);
      if ((await userCheck(ibcId, user)) || authAdmin(user)) {
        const newContributionLog = getRepository(ContributionLogEntity).create({
          date: new Date(),
          accomplishment,
          businessImpact,
          ibc: ibcId,
        });
        await newContributionLog.save();
        return newContributionLog;
      } else {
        throw new Error(errorDescriptor(ERRORS.UNAUTHORIZED));
      }
    },
    updateContributionLog: async (
      _: unknown,
      args: IUpdateContributionLogInput,
      context: IToken
    ) => {
      const { id, accomplishment, businessImpact } = args.input;
      const user = await getUser(context.token);
      if (!accomplishment && !businessImpact) {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_ON_UPDATE));
      }

      const updateContributionLog = await getRepository(
        ContributionLogEntity
      ).findOne({
        where: { id: id },
      });
      if (!updateContributionLog) {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_FOUND));
      }
      if (
        (updateContributionLog.ibc.user.id === user.id &&
          updateContributionLog.ibc.status.id !== 4) ||
        authAdmin(user)
      ) {
        if (accomplishment) {
          updateContributionLog.accomplishment = accomplishment;
        }
        if (businessImpact) {
          updateContributionLog.businessImpact = businessImpact;
        }
        const update = await updateContributionLog.save();
        return update;
      } else {
        throw new Error(errorDescriptor(ERRORS.UNAUTHORIZED));
      }
    },
    deleteContributionLog: async (
      _: unknown,
      args: IDeleteContributionLogInput,
      context: IToken
    ) => {
      const user = await getUser(context.token);
      const cl = await getRepository(ContributionLogEntity).findOneOrFail({
        where: { id: args.input.id },
      });
      if (
        (user.id === cl.ibc.user.id && cl.ibc.status.id !== 4) ||
        authAdmin(user)
      ) {
        const res = await getRepository(ContributionLogEntity).delete(
          args.input.id
        );

        if (res.affected) {
          return true;
        } else {
          return false;
        }
      } else {
        throw new Error(errorDescriptor(ERRORS.UNAUTHORIZED));
      }
    },
  },
};
export default contributioLogResolvers;
