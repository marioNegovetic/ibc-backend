import { getRepository } from "typeorm";
import {
  authAdmin,
  authReviewer,
  checkStatus,
  getUser,
  TokenUser,
  userCheck,
} from "../auth";
import { IbcEntity } from "../database/entities/IbcEntity";
import { UserEntity } from "../database/entities/UserEntity";
import { errorDescriptor, ERRORS } from "../errors";
import { IGetContributionLogInput } from "../schema/contributionLogSchema";
import { IGetDevelopmentGoalsInput } from "../schema/developmentGoalSchema";
import {
  ICreateIbcInput,
  IDeleteIbcInput,
  IGetIbcInput,
  IUpdateIbcInput,
} from "../schema/ibcSchema";
import { IGetPerformanceGoalsInput } from "../schema/performanceGoalSchema";
import { IToken } from "../schema/userSchema";
import contributioLogResolvers from "./contributionLogResolver";
import developmentGoalResolvers from "./developmentGoalResolver";
import performanceGoalResolvers from "./performanceGoalResolver";

const ibcResolvers = {
  Query: {
    getIbcs: async (_: unknown, __: unknown, context: IToken) => {
      const user = await getUser(context.token);
      const ibcs = await getRepository(IbcEntity).find();
      return ibcs;
    },
    getIbc: async (_: unknown, args: IGetIbcInput, context: IToken) => {
      const user = await getUser(context.token);
      const ibc = await getRepository(IbcEntity).findOneOrFail({
        where: { id: args.filter.id },
      });

      const ibcIdPerformance: IGetPerformanceGoalsInput = {
        filter: {
          ibcId: args.filter.id,
        },
      };

      const ibcIdDevelopment: IGetDevelopmentGoalsInput = {
        filter: {
          ibcId: args.filter.id,
        },
      };

      const ibcIdContribution: IGetContributionLogInput = {
        filter: {
          id: args.filter.id,
        },
      };
      const performanceGoals =
        await performanceGoalResolvers.Query.getPerformanceGoals(
          "",
          ibcIdPerformance
        );
      const developmentGoals =
        await developmentGoalResolvers.Query.getDevelopmentGoals(
          "",
          ibcIdDevelopment
        );
      const contributionLogs =
        await contributioLogResolvers.Query.getContributionLogs(
          "",
          ibcIdContribution
        );

      const newIbcObject = {
        ...ibc,
        performanceGoals,
        developmentGoals,
        contributionLogs,
      };

      return newIbcObject;
    },
    getIbcsReviewer: async (_: unknown, __: unknown, context: IToken) => {
      const user = await getUser(context.token);
      if (authReviewer(user)) {
        const ibcsReviewer = await getRepository(IbcEntity).find({
          where: { reviewer: user },
        });
        return ibcsReviewer;
      }
    },
    getIbcsPractitioner: async (_: unknown, __: unknown, context: IToken) => {
      const user = await getUser(context.token);
      const ibcsPractitioner = await getRepository(IbcEntity).find({
        where: { user: user },
      });
      return ibcsPractitioner;
    },
  },
  Mutation: {
    createIbc: async (_: unknown, args: ICreateIbcInput, context: IToken) => {
      const { ibcYear, annualSalary } = args.input;
      const user = await getUser(context.token);
      const loggedIn = await getRepository(UserEntity).findOneOrFail({
        where: { id: user.id },
      });

      try {
        const newIbc = getRepository(IbcEntity).create({
          ibcYear,
          annualSalary,
          status: { id: 1 },
          user: loggedIn,
        });
        await newIbc.save();
        return newIbc;
      } catch (error) {
        throw new Error(errorDescriptor(ERRORS.DUPLICATE_IBC));
      }
    },
    updateIbc: async (_: unknown, args: IUpdateIbcInput, context: IToken) => {
      const {
        id,
        ibcYear,
        midReviewDate,
        eoyReviewDate,
        status,
        reviewer,
        annualSalary,
      } = args.input;
      const user = await getUser(context.token);
      if (
        !ibcYear &&
        !midReviewDate &&
        !eoyReviewDate &&
        !status &&
        !reviewer &&
        !annualSalary
      ) {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_ON_UPDATE));
      }
      const ibc = await getRepository(IbcEntity).findOneOrFail({
        where: { id: id },
      });
      if (user.id === ibc.user.id) {
        if (ibcYear) {
          ibc.ibcYear = ibcYear;
        }
        if (annualSalary) {
          ibc.annualSalary = annualSalary;
        }
        const updatedIbc = await ibc.save();
        return updatedIbc;
      }
      if (authAdmin(user)) {
        if (ibcYear) {
          ibc.ibcYear = ibcYear;
        }
        if (reviewer) {
          ibc.reviewer = reviewer;
        }
        if (status) {
          ibc.status = status;
          if (Number(status) === 3) {
            ibc.midReviewDate = new Date();
          }
          if (Number(status) === 4) {
            ibc.eoyReviewDate = new Date();
          }
        }
        if (midReviewDate) {
          ibc.midReviewDate = midReviewDate;
        }
        if (eoyReviewDate) {
          ibc.eoyReviewDate = eoyReviewDate;
        }

        if (annualSalary) {
          ibc.annualSalary = annualSalary;
        }

        const updatedIbc = await ibc.save();
        return updatedIbc;
      }
      if (authReviewer(user)) {
        if (status) {
          ibc.status = status;
          if (Number(status) === 3) {
            ibc.midReviewDate = new Date();
          }
          if (Number(status) === 4) {
            ibc.eoyReviewDate = new Date();
          }
        }
        const updatedIbc = await ibc.save();
        return updatedIbc;
      }
    },
    deleteIbc: async (_: unknown, args: IDeleteIbcInput, context: IToken) => {
      const user = await getUser(context.token);
      const ibc = await getRepository(IbcEntity).findOneOrFail({
        where: { id: args.input.id },
      });
      if (
        (Number(user.id) === Number(ibc.user.id) &&
          Number(ibc.status.id) === 1) ||
        authAdmin(user)
      ) {
        const res = await getRepository(IbcEntity).delete(args.input.id);

        if (res.affected) {
          return true;
        } else {
          return false;
        }
      } else {
        throw new Error(errorDescriptor(ERRORS.UNAUTHORIZED));
      }
    },
  },
};

export default ibcResolvers;
