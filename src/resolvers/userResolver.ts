import { getRepository } from "typeorm";
import { config } from "dotenv";
config();
import { UserEntity } from "../database/entities/UserEntity";
import {
  ICreateUserInput,
  IGetUserInput,
  IUpdateUserInput,
  IUserLogin,
  IToken,
  IChangePassword,
} from "../schema/userSchema";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { GraphQLID } from "../utils/tsTypes";
import { authAdmin, getUser } from "../auth";
const SECRET = String(process.env.SECRET);
import { ERRORS, errorDescriptor } from "../errors";

const getToken = (
  id: GraphQLID,
  email: string,
  isAdmin: boolean,
  isReviewer: boolean
) =>
  jwt.sign(
    {
      id,
      email,
      isAdmin,
      isReviewer,
    },
    SECRET,
    { expiresIn: "1d" }
  );
const userResolvers = {
  Query: {
    getUsers: async (_: undefined, __: undefined, context: IToken) => {
      const user = await getUser(context.token);
      if (authAdmin(user)) {
        const allUsers = await getRepository(UserEntity).find();
        if (allUsers) {
          return allUsers;
        } else {
          throw new Error(errorDescriptor(ERRORS.NO_DATA_FOUND));
        }
      } else {
        throw new Error(errorDescriptor(ERRORS.RESOLVER_ONLY_FOR_ADMINS));
      }
    },
    getReviewers: async (_: undefined, __: undefined, context: IToken) => {
      const user = await getUser(context.token);
      if (authAdmin(user)) {
        const reviewers = await getRepository(UserEntity).find({
          where: { isReviewer: true, activeUser: true },
        });
        if (reviewers) {
          return reviewers;
        } else {
          throw new Error(errorDescriptor(ERRORS.NO_DATA_FOUND));
        }
      } else {
        throw new Error(errorDescriptor(ERRORS.RESOLVER_ONLY_FOR_ADMINS));
      }
    },
    getUser: async (_: unknown, args: IGetUserInput, context: IToken) => {
      await getUser(context.token);
      const user = await getRepository(UserEntity).findOne({
        where: { id: args.filter.id },
      });
      if (user) {
        return user;
      } else {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_FOUND));
      }
    },
  },
  Mutation: {
    createUser: async (_: unknown, args: ICreateUserInput, context: IToken) => {
      const {
        name,
        email,
        password,
        serviceLine,
        position,
        squadLead,
        major,
        isAdmin,
        isReviewer,
        activeUser,
      } = args.input;
      const user = await getUser(context.token);
      if (!password || password.length < 8) {
        throw new Error(errorDescriptor(ERRORS.PASSWORD_TOO_SHORT));
      }
      if (authAdmin(user)) {
        const newUser = getRepository(UserEntity).create({
          name,
          email,
          password: await bcrypt.hash(password, 12),
          serviceLine,
          position,
          squadLead,
          major,
          isAdmin,
          isReviewer,
          activeUser,
        });
        try {
          await newUser.save();
          return newUser;
        } catch (err) {
          throw new Error(errorDescriptor(ERRORS.DUPLICATE_EMAIL));
        }
      } else {
        throw new Error(errorDescriptor(ERRORS.RESOLVER_ONLY_FOR_ADMINS));
      }
    },
    updateUser: async (_: unknown, args: IUpdateUserInput, context: IToken) => {
      const {
        id,
        name,
        email,
        password,
        serviceLine,
        position,
        squadLead,
        major,
        isAdmin,
        isReviewer,
        activeUser,
      } = args.input;
      if (
        !name &&
        !email &&
        !password &&
        !serviceLine &&
        !position &&
        !squadLead &&
        !major &&
        typeof isAdmin !== "boolean" &&
        typeof isReviewer !== "boolean" &&
        typeof activeUser !== "boolean"
      ) {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_ON_UPDATE));
      }
      const authUser = await getUser(context.token);
      const user = await getRepository(UserEntity).findOne({
        where: { id: id },
      });
      if (!user) {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_FOUND));
      }
      if (authAdmin(authUser)) {
        try {
          if (name) {
            user.name = name;
          }
          if (email) {
            user.email = email;
          }
          if (password) {
            if (password.length < 8) {
              throw new Error(errorDescriptor(ERRORS.PASSWORD_TOO_SHORT));
            }
            user.password = await bcrypt.hash(password, 12);
          }
          if (serviceLine) {
            user.serviceLine = serviceLine;
          }
          if (position) {
            user.position = position;
          }
          if (squadLead) {
            user.squadLead = squadLead;
          }
          if (major) {
            user.major = major;
          }
          if (typeof isAdmin === "boolean") {
            user.isAdmin = isAdmin;
          }
          if (typeof isReviewer === "boolean") {
            user.isReviewer = isReviewer;
          }
          if (typeof activeUser === "boolean") {
            user.activeUser = activeUser;
          }
          const updatedUser = await user.save();
          return updatedUser;
        } catch {
          throw new Error(errorDescriptor(ERRORS.CANT_UPDATE_DATA_TO_DATABASE));
        }
      } else {
        throw new Error(errorDescriptor(ERRORS.RESOLVER_ONLY_FOR_ADMINS));
      }
    },
    loginUser: async (_: unknown, args: IUserLogin) => {
      const { email, password } = args.input;
      const user = await getRepository(UserEntity).findOne({
        where: { email: email },
      });
      if (!user) {
        throw new Error(errorDescriptor(ERRORS.NO_SPECIFIC_USER_FOUND));
      }

      if (!user.activeUser) {
        throw new Error(errorDescriptor(ERRORS.ACCOUNT_INACTIVE));
      }

      const match = await bcrypt.compare(password, user.password);
      if (!match)
        throw new Error(
          errorDescriptor(ERRORS.NO_SPECIFIC_USER_OR_PASSWORD_COMBINATION)
        );

      const token = getToken(
        user.id,
        user.email,
        user.isAdmin,
        user.isReviewer
      );
      return {
        token,
      };
    },
    changePassword: async (
      _: unknown,
      args: IChangePassword,
      context: IToken
    ) => {
      const { oldPassword, newPassword, confirmPassword } = args.input;
      const loggedIn = await getUser(context.token);
      const user = await getRepository(UserEntity).findOne({
        where: { id: loggedIn.id },
      });
      if (!user) {
        throw new Error(errorDescriptor(ERRORS.NO_SPECIFIC_USER_FOUND));
      }
      const match = await bcrypt.compare(oldPassword, user.password);
      if (!match) {
        throw new Error(errorDescriptor(ERRORS.WRONG_PASSWORD));
      }
      if (oldPassword === newPassword) {
        throw new Error(errorDescriptor(ERRORS.NEW_PASSWORD_MATCHING_OLD));
      }
      if (!newPassword || newPassword.length < 8) {
        throw new Error(errorDescriptor(ERRORS.PASSWORD_TOO_SHORT));
      }
      if (newPassword !== confirmPassword) {
        throw new Error(errorDescriptor(ERRORS.PASSWORDS_NOT_MATCHING));
      }
      if (newPassword) {
        user.password = await bcrypt.hash(newPassword, 12);
      }
      const updatedUser = await user.save();
      return updatedUser;
    },
  },
};

export default userResolvers;
