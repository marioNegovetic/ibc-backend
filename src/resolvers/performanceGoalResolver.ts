import { getRepository } from "typeorm";
import {
  authAdmin,
  authReviewer,
  checkStatus,
  getUser,
  userCheck,
} from "../auth";
import { PerformanceGoalEntity } from "../database/entities/PerformanceGoalEntity";
import { errorDescriptor, ERRORS } from "../errors";
import {
  ICreatePerformanceGoalInput,
  IDeletePerformanceGoalInput,
  IGetPerformanceGoalInput,
  IGetPerformanceGoalsInput,
  IUpdatePerformanceGoalInput,
} from "../schema/performanceGoalSchema";
import { IToken } from "../schema/userSchema";

const performanceGoalResolvers = {
  Query: {
    getPerformanceGoals: async (
      _: unknown,
      args: IGetPerformanceGoalsInput
    ) => {
      const allPerformanceGoals = await getRepository(
        PerformanceGoalEntity
      ).find({
        where: {
          ibc: args.filter.ibcId,
        },
      });
      return allPerformanceGoals;
    },

    getPerformanceGoal: async (
      _: unknown,
      args: IGetPerformanceGoalInput,
      context: IToken
    ) => {
      getUser(context.token);
      const performanceGoal = await getRepository(
        PerformanceGoalEntity
      ).findOne({
        where: {
          id: args.filter.id,
        },
      });
      if (performanceGoal) {
        return performanceGoal;
      } else {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_FOUND));
      }
    },
  },

  Mutation: {
    createPerformanceGoal: async (
      _: unknown,
      args: ICreatePerformanceGoalInput,
      context: IToken
    ) => {
      const { ibcId, goalDescription, startDate, endDate, supportNeeded } =
        args.input;
      const user = await getUser(context.token);
      const status = await checkStatus(ibcId);
      if (((await userCheck(ibcId, user)) && status === 1) || authAdmin(user)) {
        const newPerformanceGoal = getRepository(PerformanceGoalEntity).create({
          goalDescription,
          startDate,
          endDate,
          supportNeeded,
          ibc: ibcId,
        });
        await newPerformanceGoal.save();
        return newPerformanceGoal;
      } else {
        throw new Error(errorDescriptor(ERRORS.IBC_SUBMITTED));
      }
    },
    updatePerformanceGoal: async (
      _: unknown,
      args: IUpdatePerformanceGoalInput,
      context: IToken
    ) => {
      const {
        goalDescription,
        startDate,
        endDate,
        supportNeeded,
        midYearStatus,
        eoyYearStatus,
      } = args.input;
      const userToken = await getUser(context.token);
      if (
        !goalDescription &&
        !startDate &&
        !endDate &&
        !supportNeeded &&
        !midYearStatus &&
        !eoyYearStatus
      ) {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_ON_UPDATE));
      }

      const updatePerformance = await getRepository(
        PerformanceGoalEntity
      ).findOne({
        where: { id: args.input.id },
      });
      if (!updatePerformance) {
        throw new Error(errorDescriptor(ERRORS.NO_DATA_FOUND));
      }
      if (
        userToken.id === updatePerformance.ibc.user.id &&
        updatePerformance.ibc.status.id === 1
      ) {
        if (goalDescription) {
          updatePerformance.goalDescription = goalDescription;
        }
        if (startDate) {
          updatePerformance.startDate = startDate;
        }
        if (endDate) {
          updatePerformance.endDate = endDate;
        }
        if (supportNeeded) {
          updatePerformance.supportNeeded = supportNeeded;
        }
        const update = await updatePerformance.save();
        return update;
      }
      //admin
      if (authAdmin(userToken)) {
        if (goalDescription) {
          updatePerformance.goalDescription = goalDescription;
        }
        if (startDate) {
          updatePerformance.startDate = startDate;
        }
        if (endDate) {
          updatePerformance.endDate = endDate;
        }
        if (supportNeeded) {
          updatePerformance.supportNeeded = supportNeeded;
        }
        if (midYearStatus) {
          updatePerformance.midYearStatus = midYearStatus;
        }
        if (eoyYearStatus) {
          updatePerformance.eoyYearStatus = eoyYearStatus;
        }

        const update = await updatePerformance.save();
        return update;
      }
      if (authReviewer(userToken)) {
        if (updatePerformance.ibc.status.id === 2) {
          if (midYearStatus) {
            updatePerformance.midYearStatus = midYearStatus;
          }
        } else if (updatePerformance.ibc.status.id === 3) {
          if (eoyYearStatus) {
            updatePerformance.eoyYearStatus = eoyYearStatus;
          }
        }
        const update = await updatePerformance.save();

        return update;
      } else {
        throw new Error(errorDescriptor(ERRORS.UNAUTHORIZED));
      }
    },
    deletePerformanceGoal: async (
      _: unknown,
      args: IDeletePerformanceGoalInput,
      context: IToken
    ) => {
      const user = await getUser(context.token);
      const pg = await getRepository(PerformanceGoalEntity).findOneOrFail({
        where: { id: args.input.id },
      });
      if (
        (user.id === pg.ibc.user.id && pg.ibc.status.id === 1) ||
        authAdmin(user)
      ) {
        const res = await getRepository(PerformanceGoalEntity).delete(
          args.input.id
        );

        if (res.affected) {
          return true;
        } else {
          return false;
        }
      } else {
        throw new Error(errorDescriptor(ERRORS.UNAUTHORIZED));
      }
    },
  },
};

export default performanceGoalResolvers;
