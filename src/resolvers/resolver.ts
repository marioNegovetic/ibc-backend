import ibcResolvers from "./ibcResolver";
import performanceGoalResolvers from "./performanceGoalResolver";
import userResolvers from "./userResolver";
import contributionLogResolver from "./contributionLogResolver";
import developmentGoalResolvers from "./developmentGoalResolver";

const resolvers = [
  userResolvers,
  ibcResolvers,
  contributionLogResolver,
  performanceGoalResolvers,
  developmentGoalResolvers
];

export default resolvers;
