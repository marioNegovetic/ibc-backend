module.exports = {
  type: "postgres",
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_DATABASE,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  logging: "all",
  synchronize: false,
  entities: ["dist/database/entities/*.js"],
  migrations: ["dist/database/migrations/*.js"],
  cli: {
    entitiesDir: "dist/database/entities",
    migrationsDir: "dist/database/migrations",
  },
};
