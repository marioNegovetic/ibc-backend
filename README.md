# IBC document
The IBC document serves to recognize practitioner overall performance and contributions to iOLAP's success. IBC Goals are set at the beginning of the year while the progress is reviewed at the Midyear Checkpoint and the End of year checkpoint where IBC Results are completed annually (at the end of the year). IBC Goals are established to support iOLAP's goals for the year and state how you will contribute to the company's overall success. IBC ratings are based on your overall annual performance and completed by your Squad Lead with reviews by the executive team. IBC results include individual and corporate components.

IBC document components:

- Performance Goals: 
        Projects/initiatives related to practitioner's job performance and advancing the charter of our team/organization.
- Development Goals:
        Practitioners acquiring or enhancing specific knowledge, skills, or behaviors to help you perform more effectively in your current role and/or prepare you for new responsibilities in the future.
- Contribution log:
        Detail practitioner's accomplishments throughout the year. Practitioners must enter projects and assessments, participation in sales cycles, major accomplishments, innovation, etc.

User roles:
- Practitioner - can submit IBC, can list own IBCs
- Reviewer - can list/review IBC where  is assigned as a Reviewer, can submit/list own IBC
- Admin - can list, modify and delete all IBCs, can manage users (add, delete, assign the role(s) )


## Programs and packages installed for this application:
- Visual studio code
- PostgerSQL13
- Apollo server
- BcryptJs
- Cors
- Dotenv
- ExpressJs
- GraphQL
- Jsonwebtoken
- Pg
- Typeorm
- Reflect-metadata


## Instructions for starting the application:
- In an open directory type 'npm install' in your terminal
- Set database access data in .env file
- Create database tables by typing 'npm run typeorm --migration:run'
- Start the application by typing 'npm run dev'




